<?php

namespace Drupal\mywebsiteisonline\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MyWebsiteIsOnlineSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mywebsiteisonline.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mywebsiteisonline_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mywebsiteisonline.settings');

    $form['website_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('website_key'),
      '#title' => $this->t('Website Key'),
      '#description' => $this->t('This is used to verify your ownership.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('mywebsiteisonline.settings');

    $config
      ->set('website_key', $form_state->getValue('website_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
