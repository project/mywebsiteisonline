<?php

namespace Drupal\mywebsiteisonline\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class WebsiteController extends ControllerBase {

  public function key() {
    return new JsonResponse([
      'code' => $this->config('mywebsiteisonline.settings')->get('website_key'),
    ]);
  }

}
